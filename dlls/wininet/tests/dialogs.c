/*
 * Wininet - dialog tests
 *
 * Copyright 2008 Alexander Morozov for Etersoft
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "windows.h"
#include "wininet.h"

#include "wine/test.h"

static LONG end_thread;

static BOOL CALLBACK enum_wnd_proc(HWND hwnd, LPARAM lparam)
{
    SendMessageA(hwnd, WM_CLOSE, 0, 0);
    InterlockedIncrement(&end_thread);
    return TRUE;
}

static DWORD CALLBACK killer_thread(LPVOID param)
{
    /* Without Sleep(50) a window failed to close sometimes */
    Sleep(50);
    while (!end_thread)
        EnumThreadWindows((DWORD)param, enum_wnd_proc, 0);
    return 0;
}

static void test_InternetErrorDlg(void)
{
    HANDLE hThread;
    HINTERNET hi, hc, hr;
    DWORD ret, id;

    hi = InternetOpenA("winetest", 0, NULL, NULL, 0);
    ok(hi != NULL, "InternetOpen failed\n");
    hc = InternetConnectA(hi, "localhost", INTERNET_DEFAULT_HTTP_PORT,
            NULL, NULL, INTERNET_SERVICE_HTTP, 0, 0);
    ok(hc != NULL, "InternetConnect failed\n");
    hr = HttpOpenRequestA(hc, "GET", "/", NULL, NULL, NULL, 0, 0);
    ok(hr != NULL, "HttpOpenRequest failed\n");

    ret = InternetErrorDlg(NULL, NULL, ERROR_SUCCESS, 0, NULL);
    todo_wine
    ok(ret == ERROR_INVALID_HANDLE, "InternetErrorDlg failed\n");

    ret = InternetErrorDlg(GetDesktopWindow(), hr,
            ERROR_SUCCESS, 0, NULL);
    todo_wine
    ok(ret == ERROR_NOT_SUPPORTED, "InternetErrorDlg failed\n");

    ret = InternetErrorDlg(GetDesktopWindow(), hr,
            ERROR_FILE_NOT_FOUND, 0, NULL);
    ok(ret == ERROR_NOT_SUPPORTED, "InternetErrorDlg failed\n");

    end_thread = 0;
    hThread = CreateThread(NULL, 0, killer_thread,
            (LPVOID)GetCurrentThreadId(), 0, &id);
    ok(hThread != NULL, "create thread failed\n");
    if (NULL != hThread)
    {
        ret = InternetErrorDlg(GetDesktopWindow(), hr,
                ERROR_INTERNET_HTTP_TO_HTTPS_ON_REDIR, 0, NULL);
        ok(ret == ERROR_CANCELLED || ret == ERROR_SUCCESS,
                "InternetErrorDlg failed\n");
        InterlockedIncrement(&end_thread);
        ret = WaitForSingleObject(hThread, 3000);
        ok(ret == WAIT_OBJECT_0, "thread wait failed\n");
        CloseHandle(hThread);
    }

    end_thread = 0;
    hThread = CreateThread(NULL, 0, killer_thread,
            (LPVOID)GetCurrentThreadId(), 0, &id);
    ok(hThread != NULL, "create thread failed\n");
    if (NULL != hThread)
    {
        ret = InternetErrorDlg(GetDesktopWindow(), hr,
                ERROR_INTERNET_POST_IS_NON_SECURE, 0, NULL);
        ok(ret == ERROR_CANCELLED || ret == ERROR_SUCCESS,
                "InternetErrorDlg failed\n");
        InterlockedIncrement(&end_thread);
        ret = WaitForSingleObject(hThread, 3000);
        ok(ret == WAIT_OBJECT_0, "thread wait failed\n");
        CloseHandle(hThread);
    }

    end_thread = 0;
    hThread = CreateThread(NULL, 0, killer_thread,
            (LPVOID)GetCurrentThreadId(), 0, &id);
    ok(hThread != NULL, "create thread failed\n");
    if (NULL != hThread)
    {
        ret = InternetErrorDlg(GetDesktopWindow(), hr,
                ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED, 0, NULL);
        todo_wine
        ok(ret == ERROR_CANCELLED || ret == ERROR_SUCCESS,
                "InternetErrorDlg failed\n");
        InterlockedIncrement(&end_thread);
        ret = WaitForSingleObject(hThread, 3000);
        ok(ret == WAIT_OBJECT_0, "thread wait failed\n");
        CloseHandle(hThread);
    }

    end_thread = 0;
    hThread = CreateThread(NULL, 0, killer_thread,
            (LPVOID)GetCurrentThreadId(), 0, &id);
    ok(hThread != NULL, "create thread failed\n");
    if (NULL != hThread)
    {
        ret = InternetErrorDlg(GetDesktopWindow(), hr,
                ERROR_INTERNET_INVALID_CA, 0, NULL);
        ok(ret == ERROR_CANCELLED || ret == ERROR_SUCCESS,
                "InternetErrorDlg failed\n");
        InterlockedIncrement(&end_thread);
        ret = WaitForSingleObject(hThread, 3000);
        ok(ret == WAIT_OBJECT_0, "thread wait failed\n");
        CloseHandle(hThread);
    }

    end_thread = 0;
    hThread = CreateThread(NULL, 0, killer_thread,
            (LPVOID)GetCurrentThreadId(), 0, &id);
    ok(hThread != NULL, "create thread failed\n");
    if (NULL != hThread)
    {
        ret = InternetErrorDlg(GetDesktopWindow(), hr,
                ERROR_INTERNET_SEC_CERT_CN_INVALID, 0, NULL);
        ok(ret == ERROR_CANCELLED || ret == ERROR_SUCCESS,
                "InternetErrorDlg failed\n");
        InterlockedIncrement(&end_thread);
        ret = WaitForSingleObject(hThread, 3000);
        ok(ret == WAIT_OBJECT_0, "thread wait failed\n");
        CloseHandle(hThread);
    }

    end_thread = 0;
    hThread = CreateThread(NULL, 0, killer_thread,
            (LPVOID)GetCurrentThreadId(), 0, &id);
    ok(hThread != NULL, "create thread failed\n");
    if (NULL != hThread)
    {
        ret = InternetErrorDlg(GetDesktopWindow(), hr,
                ERROR_INTERNET_SEC_CERT_DATE_INVALID, 0, NULL);
        ok(ret == ERROR_CANCELLED || ret == ERROR_SUCCESS,
                "InternetErrorDlg failed\n");
        InterlockedIncrement(&end_thread);
        ret = WaitForSingleObject(hThread, 3000);
        ok(ret == WAIT_OBJECT_0, "thread wait failed\n");
        CloseHandle(hThread);
    }

    /* This crashes
    end_thread = 0;
    hThread = CreateThread(NULL, 0, killer_thread,
            (LPVOID)GetCurrentThreadId(), 0, &id);
    ok(hThread != NULL, "create thread failed\n");
    if (NULL != hThread)
    {
        ret = InternetErrorDlg(GetDesktopWindow(), hr,
                ERROR_INTERNET_INCORRECT_PASSWORD, 0, NULL);
        ok(ret == ERROR_CANCELLED || ret == ERROR_SUCCESS,
                "InternetErrorDlg failed\n");
        InterlockedIncrement(&end_thread);
        ret = WaitForSingleObject(hThread, 3000);
        ok(ret == WAIT_OBJECT_0, "thread wait failed\n");
        CloseHandle(hThread);
    } */

    InternetCloseHandle(hr);
    InternetCloseHandle(hc);
    InternetCloseHandle(hi);
}

START_TEST(dialogs)
{
    test_InternetErrorDlg();
}

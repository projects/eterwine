/*
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"

/**********************************************************
 * IWMPSettings methods implementation
 */

#define SETTINGS_THIS(iface) DEFINE_THIS(Player, WMPSettings, iface)

static HRESULT WINAPI WMPSettings_QueryInterface(
        IWMPSettings* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = SETTINGS_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI WMPSettings_AddRef(
        IWMPSettings* iface)
{
    Player *This = SETTINGS_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI WMPSettings_Release(
        IWMPSettings* iface)
{
    Player *This = SETTINGS_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IDispatch methods ***/
static HRESULT WINAPI WMPSettings_GetTypeInfoCount(
        IWMPSettings* iface,
        UINT *pctinfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_GetTypeInfo(
        IWMPSettings* iface,
        UINT iTInfo,
        LCID lcid,
        ITypeInfo **ppTInfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_GetIDsOfNames(
        IWMPSettings* iface,
        REFIID riid,
        LPOLESTR *rgszNames,
        UINT cNames,
        LCID lcid,
        DISPID *rgDispId)
{
    Player *This = SETTINGS_THIS(iface);
    TRACE("\n");
    return IDispatchEx_GetIDsOfNames(DISPATCHEX(This), riid, rgszNames, cNames, lcid, rgDispId);
}

static HRESULT WINAPI WMPSettings_Invoke(
        IWMPSettings* iface,
        DISPID dispIdMember,
        REFIID riid,
        LCID lcid,
        WORD wFlags,
        DISPPARAMS *pDispParams,
        VARIANT *pVarResult,
        EXCEPINFO *pExcepInfo,
        UINT *puArgErr)
{
    Player *This = SETTINGS_THIS(iface);
    TRACE("\n");
    return IDispatchEx_Invoke(DISPATCHEX(This), dispIdMember, riid, lcid, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr);
}

    /*** IWMPSettings methods ***/
static HRESULT WINAPI WMPSettings_get_isAvailable(
        IWMPSettings* iface,
        BSTR bstrItem,
        VARIANT_BOOL *pIsAvailable)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_get_autoStart(
        IWMPSettings* iface,
        VARIANT_BOOL *pfAutoStart)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_put_autoStart(
        IWMPSettings* iface,
        VARIANT_BOOL fAutoStart)
{
    TRACE("stub, returns S_OK\n");
    return S_OK;
}

static HRESULT WINAPI WMPSettings_get_baseURL(
        IWMPSettings* iface,
        BSTR *pbstrBaseURL)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_put_baseURL(
        IWMPSettings* iface,
        BSTR bstrBaseURL)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_get_defaultFrame(
        IWMPSettings* iface,
        BSTR *pbstrDefaultFrame)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_put_defaultFrame(
        IWMPSettings* iface,
        BSTR bstrDefaultFrame)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_get_invokeURLs(
        IWMPSettings* iface,
        VARIANT_BOOL *pfInvokeURLs)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_put_invokeURLs(
        IWMPSettings* iface,
        VARIANT_BOOL fInvokeURLs)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_get_mute(
        IWMPSettings* iface,
        VARIANT_BOOL *pfMute)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_put_mute(
        IWMPSettings* iface,
        VARIANT_BOOL fMute)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_get_playCount(
        IWMPSettings* This,
        long *plCount)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_put_playCount(
        IWMPSettings* iface,
        long lCount)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_get_rate(
        IWMPSettings* This,
        double *pdRate)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_put_rate(
        IWMPSettings* iface,
        double dRate)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_get_balance(
        IWMPSettings* iface,
        long *plBalance)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_put_balance(
        IWMPSettings* This,
        long lBalance)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_get_volume(
        IWMPSettings* iface,
        long *plVolume)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_put_volume(
        IWMPSettings* iface,
        long lVolume)
{
    TRACE("volume must be %ld%%\n", lVolume);
    return S_OK;
}

static HRESULT WINAPI WMPSettings_getMode(
        IWMPSettings* iface,
        BSTR bstrMode,
        VARIANT_BOOL *pvarfMode)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_setMode(
        IWMPSettings* iface,
        BSTR bstrMode,
        VARIANT_BOOL varfMode)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_get_enableErrorDialogs(
        IWMPSettings* iface,
        VARIANT_BOOL *pfEnableErrorDialogs)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPSettings_put_enableErrorDialogs(
        IWMPSettings* iface,
        VARIANT_BOOL fEnableErrorDialogs)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static const IWMPSettingsVtbl WMPSettingsVtbl =
{
    WMPSettings_QueryInterface,
    WMPSettings_AddRef,
    WMPSettings_Release,
    WMPSettings_GetTypeInfoCount,
    WMPSettings_GetTypeInfo,
    WMPSettings_GetIDsOfNames,
    WMPSettings_Invoke,
    WMPSettings_get_isAvailable,
    WMPSettings_get_autoStart,
    WMPSettings_put_autoStart,
    WMPSettings_get_baseURL,
    WMPSettings_put_baseURL,
    WMPSettings_get_defaultFrame,
    WMPSettings_put_defaultFrame,
    WMPSettings_get_invokeURLs,
    WMPSettings_put_invokeURLs,
    WMPSettings_get_mute,
    WMPSettings_put_mute,
    WMPSettings_get_playCount,
    WMPSettings_put_playCount,
    WMPSettings_get_rate,
    WMPSettings_put_rate,
    WMPSettings_get_balance,
    WMPSettings_put_balance,
    WMPSettings_get_volume,
    WMPSettings_put_volume,
    WMPSettings_getMode,
    WMPSettings_setMode,
    WMPSettings_get_enableErrorDialogs,
    WMPSettings_put_enableErrorDialogs
};

#undef SETTINGS_THIS

void Player_WMPSettings_Init(Player *This)
{
    This->lpWMPSettingsVtbl = &WMPSettingsVtbl;
}

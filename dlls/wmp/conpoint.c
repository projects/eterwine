/*
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>
#include <stdio.h>

#include "eterwmp.h"
#include "windef.h"
#include "winbase.h"

#include "wmp.h"
#include "olectl.h"

/**********************************************************
 * IConnectionPointContainer methods implementation
 */

#define CPCONT_THIS(iface) DEFINE_THIS(Player, ConnectionPointContainer, iface)

static HRESULT WINAPI ConnectionPointContainer_QueryInterface(IConnectionPointContainer *iface,
        REFIID riid, void **ppvObject)
{
    Player *This = CPCONT_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI ConnectionPointContainer_AddRef(IConnectionPointContainer *iface)
{
    Player *This = CPCONT_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI ConnectionPointContainer_Release(IConnectionPointContainer *iface)
{
    Player *This = CPCONT_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

static HRESULT WINAPI ConnectionPointContainer_EnumConnectionPoints(IConnectionPointContainer *iface,
        IEnumConnectionPoints **ppEnum)
{
    TRACE("is not implemented but returns S_OK\n");
    return S_OK;
}

static HRESULT WINAPI ConnectionPointContainer_FindConnectionPoint(IConnectionPointContainer *iface,
        REFIID riid, IConnectionPoint **ppCP)
{
    Player *This = CPCONT_THIS(iface);

    TRACE("\n");

    *ppCP = This->cpoint;
    IConnectionPoint_AddRef(This->cpoint);
    return S_OK;
}

static const IConnectionPointContainerVtbl ConnectionPointContainerVtbl = {
    ConnectionPointContainer_QueryInterface,
    ConnectionPointContainer_AddRef,
    ConnectionPointContainer_Release,
    ConnectionPointContainer_EnumConnectionPoints,
    ConnectionPointContainer_FindConnectionPoint
};

#undef CPCONT_THIS

/**********************************************************
 * IConnectionPoint methods implementation
 */

#define CPOINT_THIS(iface) DEFINE_THIS(Player, ConnectionPoint, iface)

static HRESULT WINAPI ConnectionPoint_QueryInterface(IConnectionPoint *iface,
        REFIID riid, LPVOID *ppvObject)
{
    Player *This = CPOINT_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI ConnectionPoint_AddRef(IConnectionPoint *iface)
{
    Player *This = CPOINT_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI ConnectionPoint_Release(IConnectionPoint *iface)
{
    Player *This = CPOINT_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

static HRESULT WINAPI ConnectionPoint_GetConnectionInterface(IConnectionPoint *iface, IID *pIID)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI ConnectionPoint_GetConnectionPointContainer(IConnectionPoint *iface,
        IConnectionPointContainer **ppCPC)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI ConnectionPoint_Advise(IConnectionPoint *iface, IUnknown *pUnkSink,
                                             DWORD *pdwCookie)
{
    *pdwCookie = 1;
    TRACE("Token = %d\n", *pdwCookie);
    return S_OK;
}

static HRESULT WINAPI ConnectionPoint_Unadvise(IConnectionPoint *iface, DWORD dwCookie)
{
    TRACE("Token = %d\n", dwCookie);
    return S_OK;
}

static HRESULT WINAPI ConnectionPoint_EnumConnections(IConnectionPoint *iface,
        IEnumConnections **ppEnum)
{
    TRACE("is not implemented but returns S_OK\n");
    return S_OK;
}

static const IConnectionPointVtbl ConnectionPointVtbl =
{
    ConnectionPoint_QueryInterface,
    ConnectionPoint_AddRef,
    ConnectionPoint_Release,
    ConnectionPoint_GetConnectionInterface,
    ConnectionPoint_GetConnectionPointContainer,
    ConnectionPoint_Advise,
    ConnectionPoint_Unadvise,
    ConnectionPoint_EnumConnections
};

#undef CPCONT_THIS

void Player_Connection_Init(Player *This)
{
    This->lpConnectionPointContainerVtbl = &ConnectionPointContainerVtbl;
    This->lpConnectionPointVtbl = &ConnectionPointVtbl;
}

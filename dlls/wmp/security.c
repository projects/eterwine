/*
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"

/**********************************************************
 * IClientSecurity methods implementation
 */

#define SECURITY_THIS(iface) DEFINE_THIS(Player, ClientSecurity, iface)

static HRESULT WINAPI ClientSecurity_QueryInterface(
        IClientSecurity* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = SECURITY_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI ClientSecurity_AddRef(
        IClientSecurity* iface)
{
    Player *This = SECURITY_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI ClientSecurity_Release(
        IClientSecurity* iface)
{
    Player *This = SECURITY_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IClientSecurity methods ***/
static HRESULT WINAPI ClientSecurity_QueryBlanket(
        IClientSecurity* iface,
        IUnknown *pProxy,
        DWORD *pAuthnSvc,
        DWORD *pAuthzSvc,
        OLECHAR **pServerPrincName,
        DWORD *pAuthnLevel,
        DWORD *pImpLevel,
        void **pAuthInfo,
        DWORD *pCapabilities)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI ClientSecurity_SetBlanket(
        IClientSecurity* iface,
        IUnknown *pProxy,
        DWORD AuthnSvc,
        DWORD AuthzSvc,
        OLECHAR *pServerPrincName,
        DWORD AuthnLevel,
        DWORD ImpLevel,
        void *pAuthInfo,
        DWORD Capabilities)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI ClientSecurity_CopyProxy(
        IClientSecurity* iface,
        IUnknown *pProxy,
        IUnknown **ppCopy)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}


static const IClientSecurityVtbl ClientSecurityVtbl =
{
    ClientSecurity_QueryInterface,
    ClientSecurity_AddRef,
    ClientSecurity_Release,
    ClientSecurity_QueryBlanket,
    ClientSecurity_SetBlanket,
    ClientSecurity_CopyProxy
};

#undef SECURITY_THIS

void Player_Security_Init(Player *This)
{
    This->lpClientSecurityVtbl = &ClientSecurityVtbl;
}

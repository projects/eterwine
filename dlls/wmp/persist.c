/*
 * Implementation of IPersist* interface
 *
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"

/**********************************************************
 * IPersist methods implementation
 */

#define PERSIST_THIS(iface) DEFINE_THIS(Player, Persist, iface)

static HRESULT WINAPI Persist_QueryInterface(
             IPersist* iface,
             REFIID riid,
             void **ppvObject)
{
    Player *This = PERSIST_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI Persist_AddRef(
           IPersist* iface)
{
    Player *This = PERSIST_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI Persist_Release(
           IPersist* iface)
{
    Player *This = PERSIST_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IPersist methods ***/
static HRESULT WINAPI Persist_GetClassID(
             IPersist* iface,
             CLSID *pClassID)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

    static const IPersistVtbl PersistVtbl =
{
    Persist_QueryInterface,
    Persist_AddRef,
    Persist_Release,
    Persist_GetClassID
};

#undef PERSIST_THIS

/**********************************************************
 * IPersistStream methods implementation
 */

#define PERSISTSTREAM_THIS(iface) DEFINE_THIS(Player, PersistStream, iface)

static HRESULT WINAPI PersistStream_QueryInterface(
                                             IPersistStream* iface,
                                             REFIID riid,
                                             void **ppvObject)
{
    Player *This = PERSISTSTREAM_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI PersistStream_AddRef(
                                   IPersistStream* iface)
{
    Player *This = PERSISTSTREAM_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI PersistStream_Release(
                                    IPersistStream* iface)
{
    Player *This = PERSISTSTREAM_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IPersist methods ***/
static HRESULT WINAPI PersistStream_GetClassID(
                                         IPersistStream* iface,
                                         CLSID *pClassID)
{
    TRACE("\n");
    return Persist_GetClassID((IPersist*) iface, pClassID);
}

    /*** IPersistStream methods ***/
static HRESULT WINAPI PersistStream_IsDirty(
             IPersistStream* iface)
{
    FIXME("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI PersistStream_Load(
             IPersistStream* iface,
             IStream *pStm)
{
    FIXME("is not implemented \n");
    return E_NOTIMPL;
}
static HRESULT WINAPI PersistStream_Save(
             IPersistStream* iface,
             IStream *pStm,
             BOOL fClearDirty)
{
    FIXME("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI PersistStream_GetSizeMax(
             IPersistStream* iface,
             ULARGE_INTEGER *pcbSize)
{
    FIXME("is not implemented \n");
    return E_NOTIMPL;
}

static const IPersistStreamVtbl PersistStreamVtbl =
{
    PersistStream_QueryInterface,
    PersistStream_AddRef,
    PersistStream_Release,
    PersistStream_GetClassID,
    PersistStream_IsDirty,
    PersistStream_Load,
    PersistStream_Save,
    PersistStream_GetSizeMax
};

#undef PERSISTSTREAM_THIS

/**********************************************************
 * IPersistStreamInit methods implementation
 */

#define PERSISTSTREAMINIT_THIS(iface) DEFINE_THIS(Player, PersistStreamInit, iface)

static HRESULT WINAPI PersistStreamInit_QueryInterface(IPersistStreamInit *iface,
        REFIID riid, LPVOID *ppvObject)
{
    Player *This = PERSISTSTREAMINIT_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI PersistStreamInit_AddRef(IPersistStreamInit *iface)
{
    Player *This = PERSISTSTREAMINIT_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI PersistStreamInit_Release(IPersistStreamInit *iface)
{
    Player *This = PERSISTSTREAMINIT_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

static HRESULT WINAPI PersistStreamInit_GetClassID(IPersistStreamInit *iface, CLSID *pClassID)
{
    TRACE("\n");
    return Persist_GetClassID((IPersist*) iface, pClassID);
}

static HRESULT WINAPI PersistStreamInit_IsDirty(IPersistStreamInit *iface)
{
    TRACE("\n");
    return PersistStream_IsDirty((IPersistStream*) iface);
}

static HRESULT WINAPI PersistStreamInit_Load(IPersistStreamInit *iface, LPSTREAM pStg)
{
    TRACE("\n");
    return PersistStream_Load((IPersistStream*) iface, pStg);
}

static HRESULT WINAPI PersistStreamInit_Save(IPersistStreamInit *iface, LPSTREAM pStg,
        BOOL fSameAsLoad)
{
    TRACE("\n");
    return PersistStream_Save((IPersistStream*) iface, pStg, fSameAsLoad);
}

static HRESULT WINAPI PersistStreamInit_GetSizeMax(IPersistStreamInit *iface,
        ULARGE_INTEGER *pcbSize)
{
    TRACE("\n");
    return PersistStream_GetSizeMax((IPersistStream*) iface, pcbSize);
}

static HRESULT WINAPI PersistStreamInit_InitNew(IPersistStreamInit *iface)
{
     TRACE("is not implemented but returns S_OK\n");
     return S_OK;
//     TRACE("wmp.dll: persist.c: PersistStreamInit_InitNew is not implemented\n");
//     return E_NOTIMPL;
}

static const IPersistStreamInitVtbl PersistStreamInitVtbl =
{
    PersistStreamInit_QueryInterface,
    PersistStreamInit_AddRef,
    PersistStreamInit_Release,
    PersistStreamInit_GetClassID,
    PersistStreamInit_IsDirty,
    PersistStreamInit_Load,
    PersistStreamInit_Save,
    PersistStreamInit_GetSizeMax,
    PersistStreamInit_InitNew
};

#undef PERSISTSTREAMINIT_THIS

/**********************************************************
 * IPersistHistory methods implementation
 */

#define HISTORY_THIS(iface) DEFINE_THIS(Player, PersistHistory, iface)

static HRESULT WINAPI PersistHistory_QueryInterface(
                     IPersistHistory* iface,
                     REFIID riid,
                     void **ppvObject)
{
    Player *This = HISTORY_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI PersistHistory_AddRef(IPersistHistory* iface)
{
    Player *This = HISTORY_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI PersistHistory_Release(IPersistHistory* iface)
{
    Player *This = HISTORY_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IPersist methods ***/
static HRESULT WINAPI PersistHistory_GetClassID(
             IPersistHistory* iface,
             CLSID *pClassID)
{
    FIXME("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI PersistHistory_GetPositionCookie(
             IPersistHistory* iface,
             DWORD *pdwPositioncookie)
{
    FIXME("is not implemented \n");
    return E_NOTIMPL;
}
static HRESULT WINAPI PersistHistory_LoadHistory(
    IPersistHistory* iface,
    IStream *pStream,
    IBindCtx *pbc)
{
    FIXME("is not implemented but return S_OK\n");
    return S_OK;
}

static HRESULT WINAPI PersistHistory_SaveHistory(      
    IPersistHistory* iface,
    IStream *pStream)
{
    FIXME("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI PersistHistory_SetPositionCookie(      
    IPersistHistory* iface,
    DWORD dwPositioncookie)
{
    FIXME("is not implemented \n");
    return E_NOTIMPL;
}

static const IPersistHistoryVtbl PersistHistoryVtbl =
{
    PersistHistory_QueryInterface,
    PersistHistory_AddRef,
    PersistHistory_Release,
    PersistHistory_GetClassID,
    PersistHistory_GetPositionCookie,
    PersistHistory_LoadHistory,
    PersistHistory_SaveHistory,
    PersistHistory_SetPositionCookie
};

#undef HISTORY_THIS

/**********************************************************
 * IPersistPropertyBag methods implementation
 */

#define PROPBAG_THIS(iface) DEFINE_THIS(Player, PersistPropertyBag, iface)

static HRESULT WINAPI PersistPropertyBag_QueryInterface(
        IPersistPropertyBag* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = PROPBAG_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI PersistPropertyBag_AddRef(
        IPersistPropertyBag* iface)
{
    Player *This = PROPBAG_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI PersistPropertyBag_Release(
        IPersistPropertyBag* iface)
{
    Player *This = PROPBAG_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

static HRESULT WINAPI PersistPropertyBag_GetClassID(IPersistPropertyBag *iface, CLSID *pClassID)
{
    TRACE("\n");
    return Persist_GetClassID((IPersist*) iface, pClassID);
}

    /*** IPersistPropertyBag methods ***/
static HRESULT WINAPI PersistPropertyBag_InitNew(
             IPersistPropertyBag* iface)
{
    FIXME("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI PersistPropertyBag_Load(
             IPersistPropertyBag* iface,
             IPropertyBag *pPropBag,
             IErrorLog *pErrorLog)
{
    Player *This = PROPBAG_THIS(iface);
    WCHAR str_url[] = {'u','r','l',0};
    WCHAR str_autostart[] = {'a','u','t','o','s','t','a','r','t',0};
    WCHAR str_ShowStatusBar[] = {'S','h','o','w','S','t','a','t','u','s','B','a','r',0};
    WCHAR str_volume[] = {'v','o','l','u','m','e',0};
    WCHAR str_stretchToFit[] = {'s','t','r','e','t','c','h','T','o','F','i','t',0};
    WCHAR str_baseURL[] = {'b','a','s','e','U','R','L',0};

    VARIANT pRet;
    HRESULT hres;

    TRACE("\n");

    VariantInit(&pRet);
    /* url */
    hres = IPropertyBag_Read(pPropBag, (LPOLESTR) str_url, &pRet, pErrorLog);
    if (!FAILED(hres))
    {
        TRACE("%s = \"%s\" \n", debugstr_w(str_url), debugstr_w(V_BSTR(&pRet)));
        lstrcpyW(This->url, V_BSTR(&pRet));
    }
    /* autostart */
    hres = IPropertyBag_Read(pPropBag, (LPOLESTR) str_autostart, &pRet, pErrorLog);
    if (!FAILED(hres))
    {
        FIXME("%s = ", debugstr_w(str_autostart));
        FIXME("\"%s\" \n", debugstr_w(V_BSTR(&pRet)));
    }
    /* ShowStatusBar */
    hres = IPropertyBag_Read(pPropBag, (LPOLESTR) str_ShowStatusBar, &pRet, pErrorLog);
    if (!FAILED(hres))
    {
        FIXME("%s = ", debugstr_w(str_ShowStatusBar));
        FIXME("\"%s\" \n", debugstr_w(V_BSTR(&pRet)));
    }
    /* volume */
    hres = IPropertyBag_Read(pPropBag, (LPOLESTR) str_volume, &pRet, pErrorLog);
    if (!FAILED(hres))
    {
        FIXME("%s = ", debugstr_w(str_volume));
        FIXME("\"%s\" \n", debugstr_w(V_BSTR(&pRet)));
    }
    /* stretchToFit */
    hres = IPropertyBag_Read(pPropBag, (LPOLESTR) str_stretchToFit, &pRet, pErrorLog);
    if (!FAILED(hres))
    {
        FIXME("%s = ", debugstr_w(str_stretchToFit));
        FIXME("\"%s\" \n", debugstr_w(V_BSTR(&pRet)));
    }
    /* baseURL */
    hres = IPropertyBag_Read(pPropBag, (LPOLESTR) str_baseURL, &pRet, pErrorLog);
    if (!FAILED(hres))
    {
        FIXME("%s = ", debugstr_w(str_baseURL));
        FIXME("\"%s\" \n", debugstr_w(V_BSTR(&pRet)));
    }
    VariantClear(&pRet);

    return S_OK;
}

static HRESULT WINAPI PersistPropertyBag_Save(
             IPersistPropertyBag* iface,
             IPropertyBag *pPropBag,
             BOOL fClearDirty,
             BOOL fSaveAllProperties)
{
    FIXME("is not implemented \n");
    return E_NOTIMPL;
}

static const IPersistPropertyBagVtbl PersistPropertyBagVtbl =
{
    PersistPropertyBag_QueryInterface,
    PersistPropertyBag_AddRef,
    PersistPropertyBag_Release,
    PersistPropertyBag_GetClassID,
    PersistPropertyBag_InitNew,
    PersistPropertyBag_Load,
    PersistPropertyBag_Save,
};

#undef PROPBAG_THIS

/**********************************************************
 * IPersistPropertyBag2 methods implementation
 */

#define PROPBAG2_THIS(iface) DEFINE_THIS(Player, PersistPropertyBag2, iface)

static HRESULT WINAPI PersistPropertyBag2_QueryInterface(
             IPersistPropertyBag2* iface,
             REFIID riid,
             void **ppvObject)
{
    Player *This = PROPBAG2_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI PersistPropertyBag2_AddRef(
           IPersistPropertyBag2* iface)
{
    Player *This = PROPBAG2_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI PersistPropertyBag2_Release(
           IPersistPropertyBag2* iface)
{
    Player *This = PROPBAG2_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IPersist methods ***/
static HRESULT WINAPI PersistPropertyBag2_GetClassID(
             IPersistPropertyBag2* iface,
             CLSID *pClassID)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

    /*** IPersistPropertyBag2 methods ***/
static HRESULT WINAPI PersistPropertyBag2_InitNew(
             IPersistPropertyBag2* iface)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI PersistPropertyBag2_Load(
             IPersistPropertyBag2* iface,
             IPropertyBag2 *pPropBag,
             IErrorLog *pErrorLog)
{
    Player *This = PROPBAG2_THIS(iface);

    WCHAR str_url[] = {'u','r','l',0};
    WCHAR str_autostart[] = {'a','u','t','o','s','t','a','r','t',0};
    WCHAR str_ShowStatusBar[] = {'S','h','o','w','S','t','a','t','u','s','B','a','r',0};
    WCHAR str_volume[] = {'v','o','l','u','m','e',0};
    WCHAR str_stretchToFit[] = {'s','t','r','e','t','c','h','T','o','F','i','t',0};
    WCHAR str_baseURL[] = {'b','a','s','e','U','R','L',0};
    WCHAR str_uiMode[] = {'u','i','M','o','d','e',0};
    WCHAR str_windowlessVideo[] = {'w','i','n','d','o','w','l','e','s','s','V','i','d','e','o',0};
    WCHAR str_enabled[] = {'e','n','a','b','l','e','d',0};
    WCHAR str_enableContextMenu[] = {'e','n','a','b','l','e','C','o','n','t','e','x','t','M','e','n','u',0};
    WCHAR str_fullScreen[] = {'f','u','l','l','S','c','r','e','e','n',0};
    WCHAR str_ExtentX[] = {'_','E','x','t','e','n','t','X',0};
    WCHAR str_ExtentY[] = {'_','E','x','t','e','n','t','Y',0};

    VARIANT pRet[20];
    HRESULT hres;
    ULONG count;
    ULONG count_after_get;
    int i;
    PROPBAG2 pbags[20];
    HRESULT phrError[20];

    TRACE("\n");

    VariantInit(pRet);

    hres = IPropertyBag2_CountProperties(pPropBag, &count);
    if (FAILED(hres)) {
        ERR("Failed in CountProperties\n");
        return E_FAIL;
    }
    else TRACE("count=%d\n",count);

    hres = IPropertyBag2_GetPropertyInfo(pPropBag, 0, count, pbags, &count_after_get);
    if (FAILED(hres)) {
        ERR("Failed in GetPropertyInfo\n");
        return E_FAIL;
    }
    else TRACE("count_after_get=%d\n",count_after_get);

    hres = IPropertyBag2_Read(pPropBag, count_after_get, pbags, pErrorLog, pRet, phrError);
    if (FAILED(hres)) {
        ERR("Failed in Read\n");
        return E_FAIL;
    }

    for(i = 0; i < count_after_get; i++)
    {
        if(!lstrcmpiW(pbags[i].pstrName, str_url))
        {
            TRACE("[%d] %s = \"%s\" \n", i, debugstr_w(pbags[i].pstrName), debugstr_w(V_BSTR(&pRet[i])));
            lstrcpyW(This->url, V_BSTR(&pRet[i]));
            continue;
        }
        if(!lstrcmpiW(pbags[i].pstrName, str_autostart))
        {
            /* TODO: insert your code here :) */
        }
        if(!lstrcmpiW(pbags[i].pstrName, str_ShowStatusBar))
        {
            /* TODO: insert your code here :) */
        }
        if(!lstrcmpiW(pbags[i].pstrName, str_volume))
        {
            /* TODO: insert your code here :) */
        }
        if(!lstrcmpiW(pbags[i].pstrName, str_stretchToFit))
        {
            /* TODO: insert your code here :) */
        }
        if(!lstrcmpiW(pbags[i].pstrName, str_baseURL))
        {
            /* TODO: insert your code here :) */
        }
        if(!lstrcmpiW(pbags[i].pstrName, str_uiMode))
        {
            /* TODO: insert your code here :) */
        }
        if(!lstrcmpiW(pbags[i].pstrName, str_windowlessVideo))
        {
            /* TODO: insert your code here :) */
        }
        if(!lstrcmpiW(pbags[i].pstrName, str_enabled))
        {
            /* TODO: insert your code here :) */
        }
        if(!lstrcmpiW(pbags[i].pstrName, str_enableContextMenu))
        {
            /* TODO: insert your code here :) */
        }
        if(!lstrcmpiW(pbags[i].pstrName, str_fullScreen))
        {
            /* TODO: insert your code here :) */
        }
        if(!lstrcmpiW(pbags[i].pstrName, str_ExtentX))
        {
            /* TODO: insert your code here :) */
        }
        if(!lstrcmpiW(pbags[i].pstrName, str_ExtentY))
        {
            /* TODO: insert your code here :) */
        }
        FIXME("[%d] %s is not supported\n", i, debugstr_w(pbags[i].pstrName));
    }
    VariantClear(pRet);

    return S_OK;
}

static HRESULT WINAPI PersistPropertyBag2_Save(
             IPersistPropertyBag2* iface,
             IPropertyBag2 *pPropBag,
             BOOL fClearDirty,
             BOOL fSaveAllProperties)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI PersistPropertyBag2_IsDirty(
             IPersistPropertyBag2* iface)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static const IPersistPropertyBag2Vtbl PersistPropertyBag2Vtbl =
{
    PersistPropertyBag2_QueryInterface,
    PersistPropertyBag2_AddRef,
    PersistPropertyBag2_Release,
    PersistPropertyBag2_GetClassID,
    PersistPropertyBag2_InitNew,
    PersistPropertyBag2_Load,
    PersistPropertyBag2_Save,
    PersistPropertyBag2_IsDirty
};

#undef PROPBAG2_THIS

void Player_Persist_Init(Player *This)
{
    This->lpPersistVtbl = &PersistVtbl;
    This->lpPersistStreamVtbl = &PersistStreamVtbl;
    This->lpPersistStreamInitVtbl = &PersistStreamInitVtbl;
    This->lpPersistHistoryVtbl = &PersistHistoryVtbl;
    This->lpPersistPropertyBagVtbl = &PersistPropertyBagVtbl;
    This->lpPersistPropertyBag2Vtbl = &PersistPropertyBag2Vtbl;
}

/*
 * Main module
 *
 * Copyright (C) 2008 Sinitsin Ivan (Etersoft)
 * Copyright (C) 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"
#include <initguid.h>

/* Add CLSID for Windows Media Player Classic - 22d6f312-b0f6-11d0-94ab-0080c74c7e95 */
DEFINE_GUID(CLSID_WindowsMediaPlayer6, 0x22d6f312, 0xb0f6, 0x11d0, 0x94,0xab, 0x00,0x80,0xc7,0x4c,0x7e,0x95);

LONG dll_ref = 0;

//__declspec(dllexport) BOOL __stdcall 

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    TRACE("(%p, %d, %p)\n", hinstDLL, fdwReason, lpvReserved);

    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH:
            DisableThreadLibraryCalls(hinstDLL);
            break;
        case DLL_PROCESS_DETACH:
            break;
    }

    return TRUE;
}

int WINAPI DllGetClassObject(REFCLSID rclsid, REFIID iid, LPVOID *ppv)
{
    TRACE("\n");
    *ppv = NULL;
     if (IsEqualGUID(rclsid, &CLSID_WindowsMediaPlayer) ||
         IsEqualGUID(rclsid, &CLSID_WindowsMediaPlayer6)) {
         return IClassFactory_QueryInterface((LPCLASSFACTORY)&classfactory, iid, ppv);
     }
     FIXME("Unknown class %s\n", debugstr_guid(rclsid));
     return CLASS_E_CLASSNOTAVAILABLE;
}

int WINAPI DllCanUnloadNow(void)
{
    TRACE("GLOBAL REF = %i \n",dll_ref);
    return dll_ref != 0 ? S_FALSE : S_OK;
}


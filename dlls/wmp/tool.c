/*
 * Implementation of QueryInterface method for all functions in DLL
 *
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#if 0
/* Disabled due to incomplete implementation */

#include "eterwmp.h"

HRESULT WINAPI inner_QueryInterface(
        IUnknown* iface,
        REFIID riid,
        void **ppvObject)
{
    HRESULT res;

    if (IsEqualGUID(riid, &IID_IClassFactory)) {
        TRACE("- create IClassFactory\n");
        *ppvObject = (LPVOID)iface;
        IClassFactory_AddRef(iface);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IDispatch) ||
        IsEqualGUID(riid, &IID_IUnknown) ||
        IsEqualGUID(riid, &IID_IWMPPlayer)) {
        WMPPlayerImpl *This = (WMPPlayerImpl*)iface;
        TRACE("- create IWMPPlayer\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = WMPPlayer_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IWMPPlayer_AddRef((IWMPPlayer*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IWMPPlayer2)) {
        WMPPlayer2Impl *This = (WMPPlayer2Impl*)iface;
        TRACE("- create IWMPPlayer2\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = WMPPlayer2_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IWMPPlayer2_AddRef((IWMPPlayer2*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IOleObject)) {
        OleObjectImpl *This = (OleObjectImpl*)iface;
        TRACE("- create IOleObject\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = OleObject_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IOleObject_AddRef((IOleObject*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IOleControl)) {
        OleControlImpl *This = (OleControlImpl*)iface;
        TRACE("- create IOleControl\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = OleControl_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IOleControl_AddRef((IOleControl*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IOleCommandTarget)) {
        OleCommandTargetImpl *This = (OleCommandTargetImpl*)iface;
        TRACE("- create IOleCommandTarget\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = OleCommandTarget_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IOleCommandTarget_AddRef((IOleControl*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IWMPCore)) {
        WMPCoreImpl *This = (WMPCoreImpl*)iface;
        TRACE("- create IWMPCore\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = WMPCore_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IWMPCore_AddRef((IWMPCore*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IWMPControls)) {
        WMPControlsImpl *This = (WMPControlsImpl*)iface;
        TRACE("- create IWMPControls\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = WMPControls_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IWMPControls_AddRef((IWMPControls*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IWMPError)) {
        WMPErrorImpl *This = (WMPErrorImpl*)iface;
        TRACE("- create IWMPError\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = WMPError_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IWMPError_AddRef((IWMPError*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IWMPSettings)) {
        WMPSettingsImpl *This = (WMPSettingsImpl*)iface;
        TRACE("- create IWMPSettings\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = WMPSettings_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IWMPSettings_AddRef((IWMPSettings*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IPersist)) {
        PersistImpl *This = (PersistImpl*)iface;
        TRACE("- create IPersist\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = Persist_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IPersist_AddRef((IPersist*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IPersistStream)) {
        PersistStreamImpl *This = (PersistStreamImpl*)iface;
        TRACE("- create IPersistStream\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = PersistStream_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IPersistStream_AddRef((IPersistStream*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IPersistStreamInit)) {
        PersistStreamInitImpl *This = (PersistStreamInitImpl*)iface;
        TRACE("- create IPersistStreamInit\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = PersistStreamInit_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IPersistStreamInit_AddRef((IPersistStreamInit*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IPersistHistory)) {
        PersistHistoryImpl *This = (PersistHistoryImpl*)iface;
        TRACE("- create IPersistHistory\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = PersistHistory_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IPersistHistory_AddRef((IPersistHistory*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IPersistPropertyBag)) {
        PersistPropertyBagImpl *This = (PersistPropertyBagImpl*)iface;
        TRACE("- create IPersistPropertyBag\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = PersistPropertyBag_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IPersistPropertyBag_AddRef((IPersistPropertyBag*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IPersistPropertyBag2)) {
        PersistPropertyBag2Impl *This = (PersistPropertyBag2Impl*)iface;
        TRACE("- create IPersistPropertyBag2\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = PersistPropertyBag2_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IPersistPropertyBag2_AddRef((IPersistPropertyBag2*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IConnectionPoint)) {
        ConnectionPointImpl *This = (ConnectionPointImpl*)iface;
        TRACE("- create IConnectionPoint\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = ConnectionPoint_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IConnectionPoint_AddRef((IConnectionPoint*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IConnectionPointContainer)) {
        ConnectionPointContainerImpl *This = (ConnectionPointContainerImpl*)iface;
        TRACE("- create IConnectionPointContainer\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = ConnectionPointContainer_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IConnectionPointContainer_AddRef((IConnectionPointContainer*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IPerPropertyBrowsing)) {
        PerPropertyBrowsingImpl *This = (PerPropertyBrowsingImpl*)iface;
        TRACE("- create IPerPropertyBrowsing\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = PerPropertyBrowsing_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IPerPropertyBrowsing_AddRef((IPerPropertyBrowsing*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IPointerInactive)) {
        PointerInactiveImpl *This = (PointerInactiveImpl*)iface;
        TRACE("- create IPointerInactive\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = PointerInactive_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IPointerInactive_AddRef((IPointerInactive*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IObjectSafety)) {
        ObjectSafetyImpl *This = (ObjectSafetyImpl*)iface;
        TRACE("- create IObjectSafety\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = ObjectSafety_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IObjectSafety_AddRef((IObjectSafety*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IRunnableObject)) {
        RunnableObjectImpl *This = (RunnableObjectImpl*)iface;
        TRACE("- create IRunnableObject\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = RunnableObject_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IRunnableObject_AddRef((IRunnableObject*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IViewObject)) {
        ViewObjectImpl *This = (ViewObjectImpl*)iface;
        TRACE("- create IViewObject\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = ViewObject_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IViewObject_AddRef((IViewObject*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IViewObjectEx)) {
        ViewObjectExImpl *This = (ViewObjectExImpl*)iface;
        TRACE("- create IViewObjectEx\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = ViewObjectEx_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IViewObjectEx_AddRef((IViewObject*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IClientSecurity)) {
        ClientSecurityImpl *This = (ClientSecurityImpl*)iface;
        TRACE("- create IClientSecurity\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = ClientSecurity_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IClientSecurity_AddRef((IViewObject*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IDispatchEx)) {
        DispatchExImpl *This = (DispatchExImpl*)iface;
        TRACE("- create IDispatchEx\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = DispatchEx_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IDispatchEx_AddRef((IDispatchEx*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IMediaPlayer)) {
        MediaPlayerImpl *This = (MediaPlayerImpl*)iface;
        TRACE("- create IMediaPlayer\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = MediaPlayer_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IMediaPlayer_AddRef((IMediaPlayer*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IOleWindow)) {
        OleWindowImpl *This = (OleWindowImpl*)iface;
        TRACE("- create IOleWindow\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = OleWindow_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IOleWindow_AddRef((IOleWindow*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IOleInPlaceObject)) {
        OleInPlaceObjectImpl *This = (OleInPlaceObjectImpl*)iface;
        TRACE("- create IOleInPlaceObject\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = OleInPlaceObject_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IOleInPlaceObject_AddRef((IOleInPlaceObject*)*ppvObject);
        return S_OK;
    }
    if (IsEqualGUID(riid, &IID_IOleInPlaceObjectWindowless)) {
        OleInPlaceObjectWindowlessImpl *This = (OleInPlaceObjectWindowlessImpl*)iface;
        TRACE("- create IOleInPlaceObjectWindowless\n");
        if (This == NULL || ppvObject == NULL) return E_POINTER;
        res = OleInPlaceObjectWindowless_Constructor(NULL, (LPVOID*) ppvObject);
        if (FAILED(res)) return res;
        IOleInPlaceObjectWindowless_AddRef((IOleInPlaceObjectWindowless*)*ppvObject);
        return S_OK;
    }
    return E_NOINTERFACE;
}

#ifndef __WINESRC__
char *debugstr_w(LPCWSTR str)
{
    static char buf[1024];
    WideCharToMultiByte(CP_ACP, 0, str, -1, buf, sizeof(buf), NULL, NULL);
    return buf;
}
#endif

#endif
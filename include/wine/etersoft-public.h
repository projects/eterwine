/*
 * Copyright (C) 2012, 2013 Etersoft
 * Copyright (C) 2012, 2013 Vitaly Lipatov
 * It is licensed as Wine under LGPL license.
 * If you have any questions, please mail to wine@etersoft.ru

 * To Etersoft's developers: please, change this file only in eterwine branch
 */

#ifndef __WINE_ETERSOFT_PUBLIC_H
#define __WINE_ETERSOFT_PUBLIC_H


/* eterprinting_method() return used printing method */
#define PM_DUPLEX 3
#define PM_SIDES 2
#define PM_ORIGINAL 1
static inline int eterprinting_method(void)
{
    static int current_pm = 0;
    const char *pm;
    if (current_pm)
        return current_pm;

    current_pm = PM_SIDES;

    pm = getenv("WINEPRINTING");
    if (!pm)
        return current_pm;

    if (!strcmp(pm, "duplex"))
        current_pm = PM_DUPLEX;
    else if (!strcmp(pm, "sides"))
        current_pm = PM_SIDES;
    else if (!strcmp(pm, "original"))
        current_pm = PM_ORIGINAL;

    return current_pm;
}


#endif

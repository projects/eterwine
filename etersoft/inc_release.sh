#!/bin/sh

. /etc/rpm/etersoft-build-functions
load_mod spec

#. /usr/share/eterbuild/common

SPECNAME=wine.spec
echo "Update spec: $SPECNAME"
case "$1" in
	-r)
		inc_release $SPECNAME
		;;
	-s)
		inc_subrelease $SPECNAME
		;;
	-v)
		inc_version $SPECNAME
		set_release $SPECNAME
		;;
	*)
		fatal "Run with -r/-s/-v args"
		;;
esac

TEXT="- new version $(get_version $SPECNAME)"

add_changelog_helper "$TEXT" $SPECNAME
echo "Wine from Etersoft version $(get_version $SPECNAME)-eter$(get_numrelease $SPECNAME)" >../VERSION
cd ..
autoconf
cd -
git add $SPECNAME ../VERSION ../configure ../include/config.h.in
gear-commit


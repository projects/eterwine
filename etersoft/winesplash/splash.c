/*
 * WINESPLASH - Splash screen for Wine
 *
 * Copyright (C) 2008 Illarion Ishkulov <gentro@etersoft.ru (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "splash.h"
#include <stdio.h>

#define WIDTH 450
#define HEIGHT 300
#define BORDER_WIDTH 0

int create_splash(Splash* sp, char* filename, const char* color, int yposition)
{
	unsigned int sp_attrib_mask;
	XSetWindowAttributes sp_attrib;
	XVisualInfo vTemplate;
	XVisualInfo *visualList;
	XColor defcolor;
	int visualsMatched;
	int rv;
	Pixmap mask;

	if ( ( sp->display = XOpenDisplay ( NULL ) ) == NULL )
	{
		fprintf(stderr,"Can not connect to the X server!\n");
		return 1;
	}

	/* Set default color */
	if (!color)
		color = "steelblue3";

	if (!yposition)
		yposition = 250;
	
	sp->screen_number = DefaultScreen ( sp->display );
	
	vTemplate.screen = sp->screen_number;
	visualList = XGetVisualInfo (sp->display, VisualScreenMask /*| VisualClassMask*/ , &vTemplate, &visualsMatched);
	if ( visualsMatched == 0 )
		fprintf (stderr,"(create_splash):No matching visuals\n");
	sp->colormap = 0;
	sp->colormap = XCreateColormap (sp->display, RootWindow(sp->display, sp->screen_number), visualList[0].visual, AllocNone);
	if	( sp->colormap == BadWindow	||
		  sp->colormap == BadAlloc	||
		  sp->colormap == 0)
	{
		fprintf(stderr,"(create_splash):Unable to create colormap.\n");
		return 1;
	}
	
	sp_attrib_mask = CWOverrideRedirect | CWColormap;
	sp_attrib.override_redirect = True;
	sp_attrib.colormap = sp->colormap;
	sp->window = XCreateWindow ( sp->display,
						RootWindow ( sp->display, sp->screen_number ),
						XDisplayWidth(sp->display,sp->screen_number)/2 - WIDTH/2,
						XDisplayHeight(sp->display,sp->screen_number)/2 - HEIGHT/2,
						WIDTH,
						HEIGHT,
						BORDER_WIDTH,
						visualList[0].depth,
						CopyFromParent,visualList[0].visual,
						sp_attrib_mask,
						&sp_attrib
						);
	XSetTransientForHint(sp->display, sp->window,  RootWindow ( sp->display, sp->screen_number ));

	sp_attrib_mask = CWOverrideRedirect | CWBackPixel | CWColormap;
	sp_attrib.background_pixel = WhitePixel ( sp->display, 0);
	
	sp->bar = XCreateWindow	( sp->display,
						sp->window,
						25,
						yposition,
						405,
						13,
						0,
						visualList[0].depth,
						CopyFromParent,visualList[0].visual,
						sp_attrib_mask,
						&sp_attrib
						);
	

	sp->prog = 0;

/*	rv = XParseColor(sp->display,sp->colormap, "rgbi:0.0/0.0/0.1",&(sp->bar_color)); */
	rv = XAllocNamedColor(sp->display, sp->colormap, color, &sp->bar_color, &defcolor);
	if (rv == BadColor)
	{
		fprintf(stderr,"(create_splash):Unable alloc color '%s'.\n", color);
		return 1;
	}
/*
	rv = XCreatePixmapFromData	( sp->display,
								  sp->window,
								  wine, 
								  &(sp->pic),
								  &mask,
								  NULL
								);
*/
	rv = XpmReadFileToPixmap	( sp->display,
								  sp->window,
								  filename,
								  &sp->pic,
								  &mask,
								  NULL
								);

	//backcolor.pixel = XGetPixel (image, 0, 0);

	if (rv != XpmSuccess) 
	{
		fprintf(stderr,"\n*Error*:	Unable to load pixmap.\n\n");
		sp->pic = 0;
//		return 1;
	}
	
	XSelectInput(sp->display, sp->window, ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask | FocusChangeMask);
	return 0;
}

void redraw(Splash* sp)
{
	GC gc = XCreateGC(sp->display, sp->window, 0, NULL );
	
	if (sp->pic)
		XCopyArea(sp->display, sp->pic, sp->window, gc, 0, 0, WIDTH, HEIGHT, 0, 0);

	/* draw border */
	/*
	XSetForeground(sp->display, gc, BlackPixel(sp->display, 0) );
	XDrawLine(sp->display, sp->bar, gc, 0, 0, 400,  0);
	XDrawLine(sp->display, sp->bar, gc, 0, 0,   0, 16);
	*/

	XFreeGC( sp->display, gc );
	XFlush(sp->display);

	/* Update bar with current value */
	splash_set_bar(sp, sp->prog );
}

int show_splash(Splash* sp)
{
	XMapWindow ( sp->display, sp->window );
	XMapWindow ( sp->display, sp->bar );

	redraw(sp);
	return 0;
}


int splash_set_bar(Splash* sp, int prog )
{
	int i, block;
	GC gc = XCreateGC (sp->display, sp->window, 0, NULL );

	sp->prog = prog;

/*	XSetForeground ( sp->display, gc, BlackPixel(sp->display, 0) );
	XDrawLine(sp->display, sp->bar, gc, 0,0, 380,0);
	XDrawLine(sp->display, sp->bar, gc, 0,0, 0,16);*/

	#define BLKSIZE 40
	block = ((BLKSIZE+1)*sp->prog/100)-1;

	/* show all unset box */
	
	XSetForeground ( sp->display, gc, WhitePixel(sp->display, 0) );
	for(i = block+1; i<= BLKSIZE; i++)
		XFillRectangle(sp->display, sp->bar, gc, 2+10*(i-1), 1, 8, 11);
	

	/* show all set box */
	XSetForeground ( sp->display, gc, sp->bar_color.pixel );
	for(i =0; i<= block; i++)
		XFillRectangle(sp->display, sp->bar, gc, 2+10*(i-1), 1, 8, 11);

	XFreeGC ( sp->display, gc );
	XFlush(sp->display);

	return 0;
}

int remove_splash(Splash* sp)
{
	if (sp->pic)
		XFreePixmap(sp->display, sp->pic);
	XFreeColormap(sp->display, sp->colormap);
	XDestroyWindow(sp->display, sp->bar);
	XDestroyWindow(sp->display, sp->window);
	XCloseDisplay(sp->display);
	return 0;
}

/*
 * WINESPLASH - Splash screen for Wine
 *
 * Copyright (C) 2008 Illarion Ishkulov <gentro@etersoft.ru (Etersoft)
 * Copyright (C) 2010 Vitaly Lipatov <lav@etersoft.ru (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

/*
#define VERB
#define QUITE
*/

#include "splash.h"
#include <stdio.h>
#include <stdlib.h>

Splash splash;

void do_exit(void)
{
	remove_splash(&splash);
	exit(0);
}

/* XEvents handling */
int handle_events(Splash *splash)
{
	XEvent report;
	static int clicked = 0;
	while( XPending(splash->display) > 0 )
	{
		XNextEvent(splash->display, &report);
		switch  (report.type) 
		{
			case Expose:
				redraw(splash);
				break;
			case ButtonPress:
				/* Close splash after first clock */
				do_exit();
#if 0
				/* XLib doesn't handle double click automaticaly
				 * so I used this simple double click processing.
				 * This doesn't work well.
				 */
				if (clicked > 0)
				{
					return 1;
				}
				clicked++;
				usleep(250000);
#endif
				break;
				
				
/*			case FocusOut:
				fprintf(stderr,"FocusOut\n");
				break;
			case FocusIn:
				fprintf(stderr,"FocusIn\n");
				redraw(splash);
				break;
*/
			default:
				break;
		}
	}
	clicked = 0;
	return 0;
}

// Read current position fromfile
int read_position(const char *filepath)
{
	FILE* sf;
	int i;
	sf = fopen(filepath, "r");
	if (sf==NULL)
	{
#ifdef VERB
		fprintf(stderr,"(Error): Unable open file.\n");
#endif
		// Wait for some time if interrupted
		sleep(1);
		do_exit();
	}
	if (!fscanf(sf, "%d", &i))
		i = -1;
	fclose(sf);
	return i;
}

#define USLEEP 200000


int main(int argc, char** argv)
{
	int i, l = 0;
	int cur = 0;
	int tick;
	int last = -1;
	char* filepath;
	char* picpath;
	char* pbcolor = NULL;
	int maxtime = 40; // default time in sec
	int yposition = 0;
	if(argc < 3)
	{
#ifndef QUITE
		printf("Splash screen for Wine. (c) Etersoft, 2008-2012\n\nUsage:\n    winesplash <status_file> <pic_file.xpm> [color] [max time] [y-position]\nRead man XAllocNamedColor for color names\n");
#endif
		return 1;
	} else
	{
		filepath = argv[1];
		picpath = argv[2];
	}

	/* Optional color for progress bar */
	if (argc >= 4 && argv[3] != '\0')
		pbcolor = argv[3];

	/* max splash time for auto speed */
	if (argc >= 5) {
		if (sscanf(argv[4],"%d",&i))
			maxtime = i;
	}

	/* y-position */
	if (argc >= 6) {
		if (sscanf(argv[5],"%d",&i))
			yposition = i;
	}

	i = read_position(filepath);
	if(i >= 100 || i < 0 )
		return 0;

	if ( create_splash(&splash, picpath, pbcolor, yposition))
	{
#ifdef VERB
		fprintf(stderr,"(Error): Unable create splash.\n");
#endif
		return 1;
	}

	show_splash(&splash);

	// Exit after tripled timeout
	for(tick = 0; tick < (3*maxtime*1000000/USLEEP) ; tick++)
	{
		handle_events(&splash);

		i = read_position(filepath);
		if (i > cur)
			cur = i;

		/* if not end, but we at end */
		if (cur > 99 && i < 95)
			cur = 95;

		if ( cur != last )
		{
			splash_set_bar(&splash, cur);
			last = cur;
			l = 0;
		} else
			l++;

		usleep(USLEEP);

		/* For Live progress bar */
		/* USLEEP*100 = 20 sec for full scale */
		cur += (l*USLEEP/10)/(maxtime*1000);

#ifdef VERB
		printf("tick=%d, i=%d cur=%d\n", tick, i, cur);
#endif
		/* decide to stop */
		if(i >= 100 || i < 0 )
		{
			usleep(USLEEP);
			break;
		}
	}

	do_exit();
	return 0;
}

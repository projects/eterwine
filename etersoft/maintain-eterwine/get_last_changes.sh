#!/bin/sh

WINEHQ=git://source.winehq.org/git/wine.git

# parent dir is not missed in pure
cd ../..

# Updates all our branches from winehq
if [ "$1" != "-l" ] ; then
	git checkout pure && git pull --tags $WINEHQ master || exit 1
fi

git checkout master || exit 1
git merge pure || exit 1

echo "Autoconf in master..."
autoconf -f
git commit configure -m "Build our configure"

echo "All done correctly"

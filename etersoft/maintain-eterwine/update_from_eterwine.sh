#!/bin/sh
# Copyright (c) Etersoft 2008

# ������ ���������� ��� �������������
# ��������� ��� ����� ���������� WINE@Etersoft
# �������� ����� - $CURBRANCH (��. config.in)

# ����� � ������ ������ �������, �� ������� ������������ ���� ����������
CURBRANCH=master

cd ..

bexist()
{
	[ -z "$1" ] && exit 1
	git branch | grep "$1" >/dev/null
}

# Updates branch or checkout it
bupdate()
{
	if ! bexist $1 ; then
		git checkout -b $1 origin/$1 || exit 1
	else
		git checkout $1
	fi
	git pull --rebase --tags origin $1 || exit 1
}

# parent dir is not missed in pure
cd ../

# create branches for first time
for i in pure $CURBRANCH ; do
	bupdate $i
done

git checkout $CURBRANCH || exit 1

echo "All done, stay in $CURBRANCH branch"

#!/bin/sh

check()
{
        [ "$2" != "$3" ] && echo "FATAL with '$1': result '$2' do not match with '$3'" || echo "OK for '$1' with '$2'"
}

args_to_winpath()
{
	for i in "$@" ; do
		local TP="$i"
		local TR=${i/\~/$HOME}
		if [ -r "$TR" ] ; then
			WP=$(winepath -w "$TR" 2>/dev/null)
			[ -z "$WP" ] || TP="$WP"
		fi
		echo "$TP"
	done
}

check1()
{
	check "$1" "$(args_to_winpath "$1")" "$2"
}

check1 "C:\\Program Files" "C:\\Program Files"
check1 "~/.wine/dosdevices/c:/Program Files" "C:\\Program Files"
check1 "/unix/path" "/unix/path"
check1 "~/.wine/dosdevices/c:" "C:\\"

#args_to_winpath "C:\\Program Files"  "~/.wine/dosdevices/c:/Program Files" "/unix/path" "/usr/bin/" "~/.wine/dosdevices/c:"

run_wine()
{
	for i in "$@" ; do
		echo "R: $i"
	done
}

run_wine "$(args_to_winpath "$@" )"

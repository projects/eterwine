#!/bin/sh
# Install some preneeded packages.
# See in redist.html for additional information.

# Note: remove placeholders before native dlls installing!

SYSDIR="$CROOT/windows/system32"
PROGDIR="$CROOT/Program Files/"
DOWNLOADS="$CROOT/windows/downloads"

# Remove listed dlls
predel()
{
	for i in $@ ; do
		rm -f "$SYSDIR/$i"
	done
}

failed()
{
	echo "      [ ERROR ]"
	[ "$FORCE" = "y" ] || exit 1
}

install_dll()
{
TOINSTALL=$DOWNLOADS/install_flash_player.exe
if [ -e $TOINSTALL ] ; then
	echo "Flash Player 9 NPAPI installing..."
	predel Macromed/Flash/NPSWF32.dll
	WINDOWS_VERSION=winxp $WINELOADER $TOINSTALL /S 2>/dev/null
	test -f $SYSDIR/Macromed/Flash/NPSWF32.dll || failed
fi

}

# Run install DLLs only if has admin permissions (write to C: drive)
if [ -w "$CROOT" ] && [ "$WINEMODE" != "--attach" ] ; then
	install_dll 2>>$WINEPREFIX/install.log
else
	echo "Skip dlls installing (just attaching...)"
fi


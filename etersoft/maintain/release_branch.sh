#!/bin/sh

cd ..

. ./config.in

# Script to release tarball from the branch

TEMPREPO=wine-temp-$CURDATE.repo

if [ -z "$WINEPUB_PATH" ] ; then
	. ~/Projects/git/korinf/share/eterbuild/korinf/common
	load_mod rpm
fi

ALPHA=/$ETERVERSION
if [ "$1" = "-c" ] ; then
	ALPHA=/current
	shift
fi
if [ "$1" = "-r" ] ; then
	if [ -z "$2" ] ; then
		ALPHA=/$ETERVERSION
	else
		ALPHA=/$2
		shift
	fi
	shift
fi

# CD to the root of the repo
cd ..

CURGIT=`pwd`
cd ..
git clone $CURGIT $TEMPREPO || exit 1

cd $TEMPREPO || exit 1
git pull origin $CURBRANCH

pure_rev=`git rev-parse refs/tags/$ORIGTAG`
test -n "pure_rev" || exit 1

eterhack_rev=`git rev-parse refs/remotes/origin/$CURBRANCH`
test -n "eterhack_rev" || exit 1

echo "Create logs..."
mkdir -p etersoft
git log --no-merges $pure_rev..$eterhack_rev >etersoft/ChangeLog-Etersoft || exit 1
git shortlog --no-merges  $pure_rev..$eterhack_rev >etersoft/ShortLog-Etersoft || exit 1
git diff --stat --summary -M $pure_rev..$eterhack_rev >etersoft/diffstat-Etersoft || exit 1
git add etersoft/* || exit 1

#echo "Autoconfiguring..."
#autoconf -f
git commit -a -m "Temporary commit by $0 script"
echo "Create tar..."
git archive --format=tar --prefix=$BASENAME-$ETERVERSION/ HEAD >$RPMDIR/SOURCES/$TARBALL || exit 1
pbzip2 -v -f $RPMDIR/SOURCES/$TARBALL || exit 1
mkdir -p $WINEPUB_PATH$ALPHA/sources/tarball
install -m664 $RPMDIR/SOURCES/$TARBALL.bz2 $WINEPUB_PATH$ALPHA/sources/tarball

cd ..
rm -rf $TEMPREPO

cd $CURGIT/etersoft

SPEC=$BASENAME.spec
set_version $SPEC $ETERVERSION
git commit $SPEC -m "Update release version to $ETERVERSION"

build_rpms_name $SPEC

echo Build src.rpms...
export ETERDESTSRPM=$WINEPUB_PATH$ALPHA/sources/
rpmbs -z -s $SPEC || fatal "Error with src.rpm build"


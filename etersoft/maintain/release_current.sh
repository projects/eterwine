#!/bin/sh

cd ..

. ./config.in

# Script to release tarball from the current GIT

. /etc/rpm/etersoft-build-functions
if [ -z "$WINEPUB_PATH" ] ; then
WORKDIR=/home/builder/Projects/eterbuild/functions
test -f $WORKDIR/config.in && . $WORKDIR/config.in
fi

ALPHA=/unstable
if [ "$1" = "-r" ] ; then
	if [ -z "$2" ] ; then
		ALPHA=/$ETERVERSION
	else
		ALPHA=/$2
		shift
	fi
	shift
fi

# CD to the root of the repo
cd ../
TEMPREPO=`pwd`/../wine-temp-deleteme-$CURDATE.repo
rm -rf "$TEMPREPO"

# Update for upstream repository
git checkout eterwine/pure
#git-pull origin master || exit 1
#pure_rev=`git-rev-parse refs/remotes/origin/master`
pure_rev=`git rev-parse eterwine/pure`
test -n "pure_rev" || exit 1
git checkout $CURBRANCH || exit 1

# Merge upstream changes with master branch
#git-checkout master
#git-merge pure || exit 1

# Merge master changes with $CURBRANCH branch
#git-merge master || exit 1

CURGIT=`pwd`
git clone $CURGIT $TEMPREPO || exit 1
cd $TEMPREPO || exit 1
#git checkout -b $CURBRANCH origin/$BRANCH || exit 1
echo "Jump to $CURBRANCH"
git checkout $CURBRANCH || exit 1

eterhack_rev=`git rev-parse refs/remotes/origin/$CURBRANCH`
test -n "$eterhack_rev" || exit 1

echo "Create logs..."
mkdir -p etersoft
git log --no-merges $pure_rev..$eterhack_rev >etersoft/ChangeLog-Etersoft || exit 1
git shortlog --no-merges  $pure_rev..$eterhack_rev >etersoft/ShortLog-Etersoft || exit 1
git diff --stat --summary -M $pure_rev..$eterhack_rev >etersoft/diffstat-Etersoft || exit 1
git add etersoft/* || exit 1

#echo "Autoconfiguring..."
#autoconf -f
git commit -a -m "Temporary commit by $0 script"
echo "Create tar..."
test -z "$TARBALL" && TARBALL="$PKGNAME-$ETERVERSION.tar"
git archive --format=tar --prefix=$(basename $TARBALL .tar)/ HEAD >$RPMDIR/SOURCES/$TARBALL || exit 1
#echo "
pbzip2 -v -f $RPMDIR/SOURCES/$TARBALL || exit 1
mkdir -p $WINEPUB_PATH$ALPHA/sources/tarball
install -m664 $RPMDIR/SOURCES/$TARBALL.bz2 $WINEPUB_PATH$ALPHA/sources/tarball
cd ..
rm -rf $TEMPREPO

cd $CURGIT/etersoft

SPEC=$PKGNAME.spec

ETERRELEASE=`get_release $SPEC | sed -e "s|alt|eter|g"`
TAGNAME=$ETERVERSION-$ETERRELEASE

echo "Set tag $TAGNAME"
git tag -f $TAGNAME -m "release $TAGNAME"
#set_version $SPEC $ETERVERSION
#git commit $SPEC -m "Update release version to $ETERVERSION"

build_rpms_name $SPEC

# initial attempt to fix eterbug #3650
cp -alf $WINEPUB_PATH$ALPHA/sources/tarball/$TARBALL.bz2 $WINEPUB_PATH$ALPHA/sources/tarball/$PKGNAME-$ETERVERSION-$ETERRELEASE.tar.bz2

echo Build src.rpms...
export ETERDESTSRPM=$WINEPUB_PATH$ALPHA/sources/
rpmbs -z -s $SPEC || fatal "Error with src.rpm build"

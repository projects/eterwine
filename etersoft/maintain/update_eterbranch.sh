#!/bin/sh
# Copyright (c) Etersoft 2008

# ������ ���������� ��� �������������
# ��������� ��� ����� ���������� WINE@Etersoft
# �������� ����� - $CURBRANCH (��. config.in)

# ����� � ������ ������ �������, �� ������� ������������ ���� ����������
PARENTBRANCH=eternew

cd ..

if [ -f ./config.in ] ; then
	. ./config.in
else
	echo "You need config.in file with project description"
	exit 1
fi


bexist()
{
	[ -z "$1" ] && exit 1
	git branch | grep "$1" >/dev/null
}

# Updates branch or checkout it
bupdate()
{
	if ! bexist $1 ; then
		git checkout -b $1 origin/$1 || exit 1
	else
		git checkout $1
	fi
	git pull --tags origin $1 || exit 1
}

# parent dir is not missed in pure
cd ../

# create branches for first time
for i in pure master $PARENTBRANCH $CURBRANCH ; do
	bupdate $i
done

git checkout $CURBRANCH || exit 1

# FIXME: how to detect maintainer?
if [ "$USER" != "lav" ] ; then
	echo "All done, stay in $CURBRANCH branch"
	exit 0
fi

echo "Maintainer part..."

git merge $PARENTBRANCH || exit 1

echo "$ETERNAME version $ETERVERSION" > VERSION
git commit VERSION -m "Update eterversion" || exit 1
subst "s|WINEVERSION [0-9].*|WINEVERSION $CURDATE|g" libs/wine/config.c
git commit libs/wine/config.c -m "Update source version" || exit 1

echo "Autoconf in $CURBRANCH..."
autoconf -f || exit 1
git commit configure -m "Build our configure" || exit 1

echo "All done correctly"


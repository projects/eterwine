#!/bin/sh

cd ../..

export CFLAGS="-D_FORTIFY_SOURCE=2 -g"
# --disable-tests
./configure --with-opengl || exit 1
make depend || exit 1
nice -n 19 make -j 4

#!/bin/sh

cd ..

. ./config.in

if [ "$1" = "-f" ] ; then
	FORCE="--force"
fi

git checkout $CURBRANCH || exit 1
git push $FORCE --tags origin $CURBRANCH
